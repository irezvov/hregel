import Distribution.Simple
import Distribution.PackageDescription (emptyHookedBuildInfo)
import System.Process (system)
import System.Directory (getModificationTime, doesFileExist, removeDirectory)
import Control.Monad (when, void)

main = defaultMainWithHooks $ simpleUserHooks {
  preBuild = preBuildHook,
  postClean = cleanProtoFiles
  }

isChanged :: String -> IO Bool
isChanged filename = do
  exist <- doesFileExist buildTimeFile
  if exist then compare else create
    where
      buildTimeFile = "dist/lastbuild"
      touch = writeFile buildTimeFile ""
      compare = do
        fileTime <- getModificationTime filename
        buildTime <- getModificationTime buildTimeFile
        touch
        return $ fileTime > buildTime
      create = touch >> (return True)

preBuildHook _ _ = do
  generateProto

generateProto = do
  putStrLn "Generate ProtoBuf files"
  genProto "src" "etc/hregel.proto"
  return emptyHookedBuildInfo

genProto outdir filename = do
  changed <- isChanged filename
  when changed $ void $ system $ "hprotoc -p Proto -d "
    ++ outdir ++ " " ++ filename

cleanProtoFiles _ _ _ _ = do
  void $ system "rm -r src/Proto"
