module Main where

import Concurrency.Hregel.Worker
import Concurrency.Hregel.Worker.Compute

config :: WorkerConfig String String ()
config = WorkerConfig {
    wcVertexReader = id,
    wcVertexWriter = id,
    wcEdgeReader = id,
    wcEdgeWriter = id,
    wcCompute = voteToHalt
  }

main :: IO ()
main = worker config
