module Main where

import Control.Applicative ((<$>))
import Control.Monad.State (when, join)

import Concurrency.Hregel.Worker
import Concurrency.Hregel.Worker.Compute
import qualified Concurrency.Hregel.Core.Types as T

type MaxValueCompute = Compute Int Int Int

superStep :: MaxValueCompute ()
superStep = do
  s <- superstep
  v <- value
  when (s == 1) $ sendMessages v
  maxValue <- (maximum . (v:)) <$> messages
  when (maxValue > v) $ sendMessages maxValue
  voteToHalt

sendMessages :: Int -> MaxValueCompute ()
sendMessages v = do
  setValue v
  join $ mapM_ (flip send v . T.eTo) <$> edges

config :: WorkerConfig Int Int Int
config = WorkerConfig {
    wcVertexReader = read,
    wcVertexWriter = show,
    wcEdgeReader = read,
    wcEdgeWriter = show,
    wcCompute = superStep
  }

main :: IO ()
main = worker config
