{-# LANGUAGE ScopedTypeVariables #-}

module Main where

import System.Random
import Control.Applicative ((<$>))
import Control.Monad.State
import Control.Monad.Reader

import qualified Concurrency.Hregel.Core.JsonReader as R
import qualified Concurrency.Hregel.Core.Reader as Reader
import qualified Concurrency.Hregel.Core.Types as T

import Generator.Config

type Generator = ReaderT Config (State StdGen)

nodeName :: Int -> T.VertexId
nodeName n = "node_" ++ (show n)

edge :: Int -> Generator T.EdgeDescription
edge to = do
  maxEdgeVal <- cfgMaxEdgeValue <$> ask
  (val, gen) <- gets $ randomR (0, maxEdgeVal)
  put gen
  return $ T.ED (nodeName to) $ show val

edges :: Generator [T.EdgeDescription]
edges = do
  prob <- cfgProbability <$> ask
  n <- cfgVertexNumber <$> ask
  (edgeCount, gen) <- gets
    $ randomR (0, round $ prob * (fromIntegral n) * 2)
  put gen
  to <- replicateM edgeCount $ do
    (p, innerGen) <- gets $ randomR (1, n)
    put innerGen
    return p
  mapM edge to

vertex :: Int -> Generator T.VertexDescription
vertex node = do
  maxVal <- cfgMaxVertexValue <$> ask
  (val, gen) <- gets $ randomR (0, maxVal)
  put gen
  es <- edges
  return $ T.VD (nodeName node) (show val) es

generate :: Generator [T.VertexDescription]
generate = do
  n <- cfgVertexNumber <$> ask
  mapM vertex [1..n]

main :: IO ()
main = do
  cfg <- getConfig
  gen <- getStdGen
  let res = evalState (runReaderT generate cfg) gen 
  Reader.push R.JsonReader (cfgOutputFileName cfg) res