module Generator.Config (
  Config(..),
  getConfig
) where

import System.Environment (getArgs)
import System.Console.GetOpt
import Data.Maybe (fromMaybe, listToMaybe)
import Control.Monad (join)

data Config = Config {
  cfgOutputFileName :: String,
  cfgVertexNumber :: Int,
  cfgProbability :: Float,
  cfgMaxVertexValue :: Int,
  cfgMaxEdgeValue :: Int
}

options :: [OptDescr (Config -> Config)]
options = [
  Option ['o'] ["output"]
    (OptArg outp "<file_name>")
    "output file name -- optional",
  Option ['n'] ["vertex-number"]
    (OptArg (\vs cfg -> cfg { cfgVertexNumber = getNumber 10 vs }) "NUMBER")
    "number of vertices in graph -- optional",
  Option ['p'] ["probability"]
    (OptArg (\prob cfg -> cfg { cfgProbability = getFloat 0.1 prob }) "NUMBER")
    "probability of outgoing vertex -- optional",
  Option ['v'] ["max-vertex-value"]
    (OptArg (\mvv cfg -> cfg { cfgMaxVertexValue = getNumber 10 mvv }) "NUMBER")
    "maximum vertex value -- optional",
  Option ['e'] ["max-edge-value"]
    (OptArg (\mev cfg -> cfg { cfgMaxEdgeValue = getNumber 10 mev }) "NUMBER")
    "maximum edge value -- optional"
  ]

outp :: Maybe String -> Config -> Config
outp file_name cfg = cfg { cfgOutputFileName = fromMaybe "output" file_name }

getNumber :: Int -> Maybe String -> Int
getNumber def_val str_val =
  let readToMaybe = fmap (listToMaybe . reads) in
  let (ws, _) = fromMaybe (def_val, "") $ join $ readToMaybe str_val in
  ws

getFloat :: Float -> Maybe String -> Float
getFloat def_val str_val =
  let readToMaybe = fmap (listToMaybe . reads) in
  let (ws, _) = fromMaybe (def_val, "") $ join $ readToMaybe str_val in
  ws

defaultConfig :: Config
defaultConfig = Config "output" 10 0.1 10 10

getConfig :: IO Config
getConfig = do
  args <- getArgs
  case getOpt Permute options args of
    (o, _n, []) -> return $ foldl (flip id) defaultConfig o
    (_, _, errs) -> ioError $ userError $ concat errs ++ usage
  where usage = usageInfo "" options
