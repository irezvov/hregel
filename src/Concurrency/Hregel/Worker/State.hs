module Concurrency.Hregel.Worker.State (
  Worker,
  Event(..),
  liftIO,
  runWorker,
  nodes,
  addNode,
  self,
  setSelf,
  event,
  send,
  getConn,
  addPartition,
  readVertex,
  addVertex,
  superstep,
  computationComplete,
  deliver
) where

import Data.List (find)
import Data.Function (on)
import Control.Applicative ((<$>))
import qualified Data.Map as Map
import Data.Maybe (isJust, fromJust)
import Control.Monad.State
import Control.Monad.Reader
import Control.Concurrent.Chan
import Control.Concurrent.MVar
import Control.Concurrent (forkIO)
import Data.Serialize (Serialize)

import Concurrency.Hregel.Core.Types
import qualified Concurrency.Hregel.Worker.Partition as P
import Concurrency.Hregel.Worker.Config
import qualified Concurrency.Hregel.Worker.Sender as Sender
import qualified Concurrency.Hregel.Core.Log as Log
import qualified Concurrency.Hregel.Core.Connection as Conn
import Concurrency.Hregel.Messages.NetMessage
import Concurrency.Hregel.Messages.NodeUpdate

type Worker v e m = ReaderT (WorkerConfig v e m) (StateT (WorkerState v e m) IO)

data WorkerState v e m = WS {
    wsNodes :: [Node],
    wsConn :: Conn.Connection,
    wsSelf :: Maybe Node,
    wsPartitions :: Map.Map PartitionId (P.Partition v e),
    wsNodeMap :: Map.Map PartitionId NodeId,
    wsEvents :: Chan Event,
    wsSender :: MVar (Sender.Sender m)
  }

data Event = Net NetMessage
           | EndSuperstep PartitionId Int
           | VertexData PartitionId [VertexDescription]

nodes :: Worker v e m [Node]
nodes = gets wsNodes

warning :: String -> IO ()
warning = Log.log "Worker.State" LogWarning

addNode :: Node -> Worker v e m ()
addNode node = do
  st <- get
  let ns = wsNodes st
  let nodeMap = wsNodeMap st
  let old = find isNode ns
  let ns' = filter isNode ns
  when (isJust old) $
    liftIO $ warning $ "Node already exist" ++ show node
  liftIO $ Conn.connect (wsConn st) (Worker $ nId node) $ nAddress node
  put st { wsNodes = node : ns', wsNodeMap = updateNodeMap nodeMap }
  where 
    isNode = on (==) nId node
    updateNodeMap m = 
      let f nodeMap pid = Map.insert pid (nId node) nodeMap
      in foldl f m $ nPartitions node

self :: Worker v e m (Maybe Node)
self = gets wsSelf

setSelf :: Node -> Worker v e m ()
setSelf node = modify $ \st -> st { wsSelf = Just node }

getConn :: Worker v e m Conn.Connection
getConn = gets wsConn

event :: Worker v e m Event
event = gets wsEvents >>= (liftIO . readChan)

send :: Destination -> NetMessage -> Worker v e m ()
send dest msg = do
  conn <- gets wsConn
  liftIO $ Conn.send conn dest msg

addPartition :: (Serialize m) => PartitionId -> Worker v e m ()
addPartition partId = do
  compute <- wcCompute <$> ask
  wVertVal <- wcVertexWriter <$> ask
  wEdgeVal <- wcEdgeWriter <$> ask
  events <- gets wsEvents
  sender <- gets wsSender
  let env = P.PartitionEnv {
        P.peSend = \from to msg -> 
          readMVar sender >>= \s -> Sender.send s from to msg,
        P.peStepEnd = \active ->
          writeChan events $ EndSuperstep partId active,
        P.peGetData = \vs ->
          writeChan events $ VertexData partId $
            map (writeVertex wVertVal wEdgeVal) vs,
        P.peCompute = compute
      }
  part <- liftIO $ P.newPartition env
  modify $ \st -> st {
    wsPartitions = Map.insert partId part $ wsPartitions st
  }

writeVertex :: (v -> String)
            -> (e -> String)
            -> Vertex v e
            -> VertexDescription
writeVertex wVertVal wEdgeVal (Vertex vid val edges) =
  let edges' = flip map edges $ (\(Edge to v) -> ED to $ wEdgeVal v)
  in VD vid (wVertVal val) edges'

readVertex :: VertexDescription -> Worker v e m (Vertex v e)
readVertex (VD vid val edges) = do
  vertVal <- wcVertexReader <$> ask
  edgeVal <- wcEdgeReader <$> ask
  let edges' = flip map edges $ (\(ED to v) -> Edge to $ edgeVal v)
  return $ Vertex vid (vertVal val) edges'

addVertex :: PartitionId -> Vertex v e -> Worker v e m ()
addVertex partId vert = do
  part <- Map.lookup partId <$> gets wsPartitions
  when (isJust part) $ 
    liftIO $ P.addVertex (fromJust part) vert

superstep :: (Serialize m) => Worker v e m ()
superstep = do
  sender <- gets wsSender
  isEmpty <- liftIO $ isEmptyMVar sender
  when isEmpty $ do
    ns <- gets wsNodes
    conn <- gets wsConn
    sender' <- liftIO $ Sender.sender conn ns
    liftIO $ putMVar sender sender'
  parts <- Map.elems <$> gets wsPartitions
  liftIO $ Log.logWith "Worker.State" LogInfo "Start superstep. Partitions: " (length parts)
  liftIO $ mapM_ P.superstep parts

computationComplete :: Worker v e m ()
computationComplete = do
  parts <- Map.elems <$> gets wsPartitions
  liftIO $ Log.log "Worker.State" LogInfo "End computation"
  liftIO $ mapM_ P.getData parts

deliver :: (Serialize m) => PartitionId -> [NodeUpdate] -> Worker v e m ()
deliver partId upds = do
  part <- Map.lookup partId <$> gets wsPartitions
  when (isJust part)
    $ liftIO $ forM_ upds $ P.deliver (fromJust part)

runWorker :: (Serialize m)
          => Conn.Connection
          -> WorkerConfig v e m 
          -> Worker v e m ()
          -> IO ()
runWorker conn cfg comp = do
  events <- newChan
  sender <- newEmptyMVar
  forkIO $ forever $ do
    msg <- Conn.receive conn
    writeChan events $ Net msg
  evalStateT (runReaderT comp cfg) $ WS {
    wsNodes = [],
    wsConn = conn,
    wsSelf = Nothing,
    wsPartitions = Map.empty,
    wsNodeMap = Map.empty,
    wsEvents = events,
    wsSender = sender
  }
