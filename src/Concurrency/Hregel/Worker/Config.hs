module Concurrency.Hregel.Worker.Config (
  Config(..),
  WorkerConfig(..),
  getConfig
  ) where

import System.Environment (getArgs)
import System.Console.GetOpt
import Control.Monad (when)

import Concurrency.Hregel.Worker.Compute (Compute)

data WorkerConfig v e m = WorkerConfig {
    wcVertexReader :: String -> v,
    wcVertexWriter :: v -> String,
    wcEdgeReader :: String -> e,
    wcEdgeWriter :: e -> String,
    wcCompute :: Compute v e m ()
  }

data Config = Config {
  cfgMaster :: String
  }

options :: [OptDescr (Config -> Config)]
options = [ Option ['m'] ["master"]
            (ReqArg (\addr cfg -> cfg { cfgMaster = addr }) "ADDR:PORT")
            "address of master node"
          ]

getConfig :: IO Config
getConfig = do
  args <- getArgs
  when (null args) $ ioError $ userError usage
  case getOpt Permute options args of
    (o, _n, []) -> return $ foldl (flip id) (Config "") o
    (_, _, errs) -> ioError $ userError $ concat errs ++ usage
  where usage = usageInfo "" options
