module Concurrency.Hregel.Worker.Sender (
  Sender,
  sender,
  send
) where

import qualified Data.Map as Map
import Data.Maybe (isJust, fromJust)
import Control.Applicative ((<$>))
import Control.Monad.Reader
import Control.Concurrent.Chan
import Control.Concurrent (forkIO)
import Data.Serialize

import Concurrency.Hregel.Core.Types hiding (Message)
import Concurrency.Hregel.Core.Partitioner
import qualified Concurrency.Hregel.Core.Connection as Conn
import Concurrency.Hregel.Messages.NetMessage
import Concurrency.Hregel.Messages.NodeUpdate

type SenderM m = ReaderT (SenderConfig m) IO

data SenderConfig m = Config {
    scConn :: Conn.Connection,
    scPartitions :: Map.Map PartitionId NodeId,
    scInput :: Chan (Command m)
  }

data Sender m = Sender { sInput :: Chan (Command m) }

data Command m = Send VertexId VertexId m

sender :: (Serialize m)
       => Conn.Connection
       -> [Node]
       -> IO (Sender m)
sender conn nodes = do
  inp <- newChan
  let cfg = Config {
        scConn = conn,
        scPartitions = makePartitionsMap nodes,
        scInput = inp
      }
  forkIO $ runReaderT routine cfg
  return $ Sender inp

send :: Sender m -> VertexId -> VertexId -> m -> IO ()
send Sender { sInput = inp } from to msg = 
  writeChan inp $ Send from to msg

makePartitionsMap :: [Node] -> Map.Map PartitionId NodeId
makePartitionsMap = Map.fromList . concatMap f
  where 
    f Node { nId = nid, nPartitions = parts } =
      map (flip (,) nid) parts

routine :: (Serialize m) => SenderM m ()
routine = forever $ do
  inp <- scInput <$> ask
  cmd <- liftIO $ readChan inp
  case cmd of
    Send from to msg -> handleSend from to msg
    --_ -> error "Not implemented"

handleSend :: (Serialize m) => VertexId -> VertexId -> m -> SenderM m ()
handleSend from to msg = do
  parts <- scPartitions <$> ask
  conn <- scConn <$> ask
  let count = length $ Map.keys parts
  let part = defaultPartition to count
  let node = Map.lookup part parts
  when (isJust node) $ do
    let upd = NodeUpdate from part Message $ packMessage from msg
    let netMsg = NodeUpdateReqMsg $ NodeUpdateReq part [upd]
    liftIO $ Conn.send conn (Worker $ fromJust node) netMsg