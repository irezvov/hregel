module Concurrency.Hregel.Worker.Compute (
  Compute,
  VertexState(..),
  VertexEnv(..),
  Edge(..),
  compute,
  voteToHalt,
  messages,
  self,
  value,
  setValue,
  edges,
  send,
  superstep
) where

import Control.Applicative
import Control.Monad.State
import Control.Monad.Reader
import Control.Concurrent.Chan ()

import Concurrency.Hregel.Core.Types

data VertexEnv m = VertexEnv {
    veSend :: VertexId -> VertexId -> m -> IO (),
    veInbox :: [m],
    veSuperstep :: Int
  }

data VertexState v e = VertexState {
    vsId :: VertexId,
    vsValue :: v,
    vsEdges :: [Edge e],
    vsHalted :: Bool
  }

type Compute v e m = 
  ReaderT (VertexEnv m) (StateT (VertexState v e) IO)

compute :: VertexEnv m -> VertexState v e -> Compute v e m ()
           -> IO (VertexState v e)
compute env initState comp = execStateT (runReaderT comp env) initState

voteToHalt :: Compute v e m ()
voteToHalt = modify $ \st -> st { vsHalted = True }

messages :: Compute v e m [m]
messages = veInbox <$> ask

self :: Compute v e m VertexId
self = gets vsId

value :: Compute v e m v
value = gets vsValue

setValue :: v -> Compute v e m ()
setValue v = modify $ \st -> st { vsValue = v }

edges :: Compute v e m [Edge e]
edges = gets vsEdges

send :: VertexId -> m -> Compute v e m ()
send vid msg = do
  send' <- veSend <$> ask
  self' <- self
  liftIO $ send' self' vid msg

superstep :: Compute v e m Int
superstep = veSuperstep <$> ask
