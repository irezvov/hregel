module Concurrency.Hregel.Worker.Partition (
  Partition,
  PartitionEnv(..),
  newPartition,
  addVertex,
  superstep,
  getData,
  deliver
) where

import qualified Data.Map as Map
import Control.Applicative ((<$>))
import Control.Monad.State
import Control.Monad.Reader
import Control.Concurrent.Chan
import Control.Concurrent (forkIO)
import Data.Serialize

import Concurrency.Hregel.Core.Types
import Concurrency.Hregel.Worker.Compute (
  VertexState(..), Compute, VertexEnv(..), compute)
import qualified Concurrency.Hregel.Core.Log as Log
import qualified Concurrency.Hregel.Messages.NodeUpdate as NU

data Partition v e = Partition (Chan (Command v e))

data PartitionState v e m = PS {
    psVertices :: Map.Map VertexId (VertexState v e),
    psSuperstep :: Int,
    psMessages :: Map.Map VertexId [m]
  }

data Command v e = StartSuperstep
                 | AddVertex (Vertex v e)
                 | Deliver NU.NodeUpdate
                 | GetData

data PartitionEnv v e m = PartitionEnv {
    peSend :: VertexId -> VertexId -> m -> IO (),
    peStepEnd :: Int -> IO (),
    peGetData :: [Vertex v e] -> IO (),
    peCompute :: Compute v e m ()
  }

type PartitionM v e m =
  ReaderT (PartitionEnv v e m) (StateT (PartitionState v e m) IO)

initState :: PartitionState v e m
initState = PS Map.empty 1 Map.empty

newPartition :: (Serialize m) => PartitionEnv v e m -> IO (Partition v e)
newPartition env = do
  inp <- newChan
  Log.log "Worker.Partition" LogInfo "Start new partition"
  forkIO $ evalStateT (runReaderT (routine inp) env) initState
  return $ Partition inp

routine :: (Serialize m) => Chan (Command v e) -> PartitionM v e m ()
routine inp = forever $ do
  cmd <- liftIO $ readChan inp
  case cmd of
    AddVertex vertex ->
      modify $ \st ->
        st {
          psVertices = 
            let i = vId vertex
                v = vertexToState vertex
                vertices = psVertices st
            in Map.insert i v vertices
        }
    StartSuperstep -> do
      step <- gets psSuperstep
      liftIO $ Log.log "Worker.Partition" LogDebug "Start superstep"
      end <- peStepEnd <$> ask
      active <- makeStep
      liftIO $ Log.logWith "Worker.Partition" LogDebug "End superstep" active
      liftIO $ end active
      modify $ \st -> st { psSuperstep = step + 1 }
    Deliver NU.NodeUpdate {
        NU.nuUpdateType = updType,
        NU.nuUpdate = upd
      } ->
      case updType of
        NU.Message -> do
         let (to, msg) = NU.unpackMessage upd
         modify $ \st ->
           let messages = Map.insertWith (++) to [msg] $ psMessages st
           in st { psMessages = messages }
        _ -> return ()
    GetData -> do
      vs <- Map.elems <$> gets psVertices
      sendData <- peGetData <$> ask
      liftIO $ sendData $ map stateToVertex vs

-- return True if vertex halted
calcVertex :: Map.Map VertexId [m]
           -> VertexState v e
           -> PartitionM v e m Bool
calcVertex messages v = do
  let inbox = Map.findWithDefault [] (vsId v) messages
  if (not $ null inbox) || (not $ vsHalted v)
  then calc inbox
  else return True
  where 
    calc inbox = do
      send' <- peSend <$> ask
      step <- gets psSuperstep
      comp <- peCompute <$> ask
      vs <- gets psVertices
      let env = VertexEnv send' inbox step
      v' <- liftIO $ compute env (v { vsHalted = False }) comp
      modify $ \st -> st { psVertices = Map.insert (vsId v) v' vs }
      return $ vsHalted v'

makeStep :: PartitionM v e m Int
makeStep = do
  vs <- Map.elems <$> gets psVertices
  -- TODO: fix possible delivery messages from next superstep
  msgs <- gets psMessages
  modify $ \st -> st { psMessages = Map.empty }
  length . (filter not) <$> mapM (calcVertex msgs) vs

vertexToState :: Vertex v e -> VertexState v e
vertexToState (Vertex vertexId v es) =
  VertexState vertexId v es False

stateToVertex :: VertexState v e -> Vertex v e
stateToVertex (VertexState vertexId v es _) =
  Vertex vertexId v es

addVertex :: Partition v e -> Vertex v e -> IO ()
addVertex (Partition inp) v =
  writeChan inp $ AddVertex v

superstep :: Partition v e -> IO ()
superstep (Partition inp) = 
  writeChan inp StartSuperstep

deliver :: Partition v e -> NU.NodeUpdate -> IO ()
deliver (Partition inp) upd = 
  writeChan inp $ Deliver upd

getData :: Partition v e -> IO ()
getData (Partition inp) =
  writeChan inp GetData
