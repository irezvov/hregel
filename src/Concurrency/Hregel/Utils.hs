module Concurrency.Hregel.Utils (
  getFreePort,
  getAddress,
  utcToTimestamp,
  timestampToUtc
) where

import Network.Socket
import Network.Info
import Control.Applicative
import Data.Time.Clock 
import Data.Time.Clock.POSIX
import Data.Word

getFreePort :: IO PortNumber
getFreePort = do
    let hints = defaultHints { addrFamily = AF_INET }
    addr <- head <$> getAddrInfo (Just hints) (Just "localhost") Nothing
    s <- socket (addrFamily addr) Stream defaultProtocol
    bind s $ addrAddress addr
    port <- socketPort s
    sClose s
    return port

getAddress :: IO String
getAddress =
    head . map (show . ipv4) . filter ((/= "lo") . name) <$> getNetworkInterfaces

timestampToUtc :: Word64 -> UTCTime
timestampToUtc = posixSecondsToUTCTime . fromRational . (/1000) . fromIntegral 

utcToTimestamp :: UTCTime -> Word64
utcToTimestamp = round . (* 1000) . utcTimeToPOSIXSeconds