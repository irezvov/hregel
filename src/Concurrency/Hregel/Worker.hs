{-# LANGUAGE NoMonomorphismRestriction #-}
module Concurrency.Hregel.Worker (
  WorkerConfig(..),
  worker
  ) where

import Control.Monad (forever)
import Control.Applicative ((<$>), (<*>))
import Data.Serialize (Serialize)

import Concurrency.Hregel.Core.Types
import Concurrency.Hregel.Core.Connection (connection, connect)
import Concurrency.Hregel.Utils
import Concurrency.Hregel.Worker.Config
import Concurrency.Hregel.Messages.NetMessage
import qualified Concurrency.Hregel.Core.Log as Log
import Concurrency.Hregel.Worker.State

handleNetMessage :: (Serialize m) => NetMessage -> Worker v e m ()
handleNetMessage msg = case msg of
  HelloMasterResMsg (HelloMasterRes node nodeList _) -> do
    conn <- getConn
    liftIO $ Log.addComponentEx "Worker" (nId node) conn
    liftIO $ Log.log "Worker" LogInfo "Received HelloMasterResMsg"
    mapM_ addNode nodeList
    let message = HelloNodeReqMsg $ HelloNodeReq node
    mapM_ (flip send message . Worker . nId) nodeList
    mapM_ addPartition $ nPartitions node
    setSelf node
  HelloNodeReqMsg (HelloNodeReq node) -> do
    addNode node
    liftIO $ Log.logWith "Worker" LogInfo "Node connected" node
  PingReqMsg _ -> do
    Just node <- self
    send Master $ PongResMsg $ PongRes $ nId node
  InputReqMsg (InputReq partId vs) -> do
    vs' <- mapM readVertex vs
    liftIO $ Log.log "Worker" LogInfo
      ("Add " ++ show (length vs') ++ " vertices to " ++ show partId)
    mapM_ (addVertex partId) vs'
  SuperstepReqMsg _ ->
    superstep
  ComputationCompleteReqMsg _ -> 
    computationComplete
  NodeUpdateReqMsg (NodeUpdateReq partId upds) ->
    deliver partId upds
  _ ->
    liftIO $ Log.logWith "Worker" LogWarning "Unknown message" msg

logWith :: Show a => LogLevel -> String -> a -> IO ()
logWith = Log.logWith "Worker"

workerBody :: (Serialize m) => String -> Worker v e m ()
workerBody fullAddress = do 
  send Master $ HelloMasterReqMsg $ HelloMasterReq fullAddress
  forever $ do
    e <- event
    case e of
      Net msg -> do
        liftIO $ logWith LogDebug "Received message" msg
        handleNetMessage msg
      EndSuperstep partId active -> do
        liftIO $ logWith LogInfo "End superstep" (partId, active)
        Just node <- self
        let msg = EndSuperstepResMsg . EndSuperstepRes (nId node)
        send Master $ msg active
      VertexData partId vs -> do
        liftIO $ logWith LogInfo "Recieve data from partition" vs
        send Master $ InputReqMsg $ InputReq partId vs

worker :: (Serialize m) => WorkerConfig v e m -> IO ()
worker config = do
  cfg <- getConfig
  (addr, port) <- (,) <$> getAddress <*> getFreePort
  let fullAddress = addr ++ ":" ++ show port
  let master_addr = cfgMaster cfg

  conn <- connection fullAddress
  Log.addComponent "Worker"
  connect conn Master master_addr
  Log.logWith "Worker" LogInfo "Connecting from: " fullAddress
  Log.logWith "Worker" LogInfo "Master address: " master_addr
  runWorker conn config $ workerBody fullAddress
