module Concurrency.Hregel.Messages.NetMessage (
  NetMessage(..),
  HelloMasterReq(..),
  HelloMasterRes(..),
  HelloNodeReq(..),
  LogReq(..),
  PingReq(..),
  PongRes(..),
  InputReq(..),
  EndInputReq(..),
  SuperstepReq(..),
  EndSuperstepRes(..),
  ComputationCompleteReq(..),
  NodeUpdateReq(..)
) where

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as Lazy
import Control.Applicative ((<$>))

import Text.ProtocolBuffers.Basic (defaultValue)
import Text.ProtocolBuffers.WireMessage (messageGet)

import Concurrency.Hregel.Messages.Message
import Concurrency.Hregel.Messages.HelloMasterReq
import Concurrency.Hregel.Messages.HelloMasterRes
import Concurrency.Hregel.Messages.HelloNodeReq
import Concurrency.Hregel.Messages.LogReq
import Concurrency.Hregel.Messages.PingReq
import Concurrency.Hregel.Messages.PongRes
import Concurrency.Hregel.Messages.InputReq
import Concurrency.Hregel.Messages.EndInputReq
import Concurrency.Hregel.Messages.SuperstepReq
import Concurrency.Hregel.Messages.EndSuperstepRes
import Concurrency.Hregel.Messages.ComputationCompleteReq
import Concurrency.Hregel.Messages.NodeUpdateReq

import qualified Proto.Hregel.Message as M
import Proto.Hregel.Message.Type

data NetMessage = HelloMasterReqMsg HelloMasterReq
                | HelloMasterResMsg HelloMasterRes
                | HelloNodeReqMsg HelloNodeReq
                | LogReqMsg LogReq
                | PingReqMsg PingReq
                | PongResMsg PongRes
                | InputReqMsg InputReq
                | EndInputReqMsg EndInputReq
                | SuperstepReqMsg SuperstepReq
                | EndSuperstepResMsg EndSuperstepRes
                | ComputationCompleteReqMsg ComputationCompleteReq
                | NodeUpdateReqMsg NodeUpdateReq
  deriving (Eq, Show)

packNetMessage :: (Message a) => Type -> a -> BS.ByteString
packNetMessage msgType msg = defaultPack $ defaultValue {
      M.messageBody = Lazy.fromStrict $ pack msg,
      M.type' = msgType
    }

instance Message NetMessage where
  pack (HelloMasterReqMsg msg) = packNetMessage HELLO_MASTER_REQ msg
  pack (HelloMasterResMsg msg) = packNetMessage HELLO_MASTER_RES msg
  pack (HelloNodeReqMsg msg) = packNetMessage HELLO_NODE_REQ msg
  pack (LogReqMsg msg) = packNetMessage LOG_REQ msg
  pack (PingReqMsg msg) = packNetMessage PING_REQ msg
  pack (PongResMsg msg) = packNetMessage PONG_RES msg
  pack (InputReqMsg msg) = packNetMessage INPUT_REQ msg
  pack (EndInputReqMsg msg) = packNetMessage END_INPUT_REQ msg
  pack (SuperstepReqMsg msg) = packNetMessage SUPERSTEP_REQ msg
  pack (EndSuperstepResMsg msg) = packNetMessage END_SUPERSTEP_RES msg
  pack (ComputationCompleteReqMsg msg) =
    packNetMessage COMPUTATION_COMPLETE_REQ msg
  pack (NodeUpdateReqMsg msg) =
    packNetMessage NODE_UPDATE_REQ msg
  
  unpack bin = do
    (M.Message msgType _ body, _rest) <- messageGet $ Lazy.fromStrict bin 
    let make ctr = ctr <$> (unpack $ Lazy.toStrict body)
    case msgType of
      HELLO_MASTER_REQ         -> make HelloMasterReqMsg
      HELLO_MASTER_RES         -> make HelloMasterResMsg
      HELLO_NODE_REQ           -> make HelloNodeReqMsg
      LOG_REQ                  -> make LogReqMsg
      PING_REQ                 -> make PingReqMsg
      PONG_RES                 -> make PongResMsg
      INPUT_REQ                -> make InputReqMsg
      END_INPUT_REQ            -> make EndInputReqMsg
      SUPERSTEP_REQ            -> make SuperstepReqMsg
      END_SUPERSTEP_RES        -> make EndSuperstepResMsg
      COMPUTATION_COMPLETE_REQ -> make ComputationCompleteReqMsg
      NODE_UPDATE_REQ          -> make NodeUpdateReqMsg
      _                        -> Left "Bad message"
