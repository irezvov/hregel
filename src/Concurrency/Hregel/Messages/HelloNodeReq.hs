module Concurrency.Hregel.Messages.HelloNodeReq (
  HelloNodeReq(..)
  ) where

import Text.ProtocolBuffers.Basic (defaultValue)

import Concurrency.Hregel.Core.Types (Node(..))
import Concurrency.Hregel.Messages.Message
import Concurrency.Hregel.Messages.Types

import qualified Proto.Hregel.HelloNodeReq as Proto

data HelloNodeReq = HelloNodeReq { hnSender :: Node }
  deriving (Show, Eq)

instance Message HelloNodeReq where
  pack (HelloNodeReq node) = defaultPack $ defaultValue {
      Proto.sender = nodeToProto node
    }
  unpack = defaultUnpack $ \(Proto.HelloNodeReq node) ->
    HelloNodeReq $ protoToNode node
