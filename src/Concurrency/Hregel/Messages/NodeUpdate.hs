module Concurrency.Hregel.Messages.NodeUpdate (
  NodeUpdate(..),
  updateToProto,
  protoToUpdate,
  packMessage,
  unpackMessage,
  UT.UpdateType(..)
) where

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as Lazy
import Text.ProtocolBuffers.Basic (
    uFromString, uToString
  )
import Text.ProtocolBuffers.WireMessage (messagePut, messageGet)
import Data.Serialize

import Concurrency.Hregel.Core.Types hiding (Message)
import qualified Proto.Hregel.NodeUpdate as Proto
import qualified Proto.Hregel.UpdateType as UT
import qualified Proto.Hregel.MessageUpdate as MU

data NodeUpdate = NodeUpdate {
    nuFrom :: VertexId,
    nuPartition :: PartitionId,
    nuUpdateType :: UT.UpdateType,
    nuUpdate :: BS.ByteString
  }
  deriving (Show, Eq)

packMessage :: (Serialize m) => VertexId -> m -> BS.ByteString
packMessage to msg = 
  let protoMsg = MU.MessageUpdate {
    MU.to = uFromString to,
    MU.data' = Lazy.fromStrict $ encode msg
  }
  in Lazy.toStrict $ messagePut protoMsg

unpackMessage :: (Serialize m) => BS.ByteString -> (VertexId, m)
unpackMessage inp =
  let protoMsg = messageGet $ Lazy.fromStrict inp
      Right (MU.MessageUpdate to msgBody, _) = protoMsg
      Right msg = decode $ Lazy.toStrict msgBody
  in (uToString to, msg)

updateToProto :: NodeUpdate -> Proto.NodeUpdate
updateToProto (NodeUpdate from part updType upd) =
  Proto.NodeUpdate {
    Proto.from = uFromString from,
    Proto.partition = fromIntegral part,
    Proto.type' = updType,
    Proto.updateBody = Lazy.fromStrict $ upd
  }

protoToUpdate :: Proto.NodeUpdate -> NodeUpdate
protoToUpdate = undefined