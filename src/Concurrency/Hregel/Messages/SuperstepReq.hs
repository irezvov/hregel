module Concurrency.Hregel.Messages.SuperstepReq (
  SuperstepReq(..)
) where

import Text.ProtocolBuffers.Basic (defaultValue)

import Concurrency.Hregel.Messages.Message
import qualified Proto.Hregel.SuperstepReq as Proto

data SuperstepReq = SuperstepReq
  deriving (Show, Eq)

fromProto :: Proto.SuperstepReq -> SuperstepReq
fromProto = const SuperstepReq

instance Message SuperstepReq where
  pack SuperstepReq = 
    defaultPack (defaultValue :: Proto.SuperstepReq)
  unpack = defaultUnpack fromProto
