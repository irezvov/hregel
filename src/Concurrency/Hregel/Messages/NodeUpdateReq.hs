module Concurrency.Hregel.Messages.NodeUpdateReq (
  NodeUpdateReq(..)
) where

import Data.Sequence (fromList)
import Data.Foldable (toList)
import Text.ProtocolBuffers.Basic (defaultValue)

import Concurrency.Hregel.Core.Types hiding (Message)
import Concurrency.Hregel.Messages.Message
import Concurrency.Hregel.Messages.NodeUpdate

import qualified Proto.Hregel.NodeUpdateReq as Proto

data NodeUpdateReq = NodeUpdateReq {
    nurFrom :: PartitionId,
    nurUpdates :: [NodeUpdate]
  }
  deriving (Eq, Show)

instance Message NodeUpdateReq where
  pack (NodeUpdateReq from upds) = defaultPack $ defaultValue {
      Proto.from = fromIntegral from,
      Proto.updates = fromList $ map updateToProto upds
    }

  unpack = defaultUnpack $ \(Proto.NodeUpdateReq from upds) ->
    NodeUpdateReq {
      nurFrom = fromIntegral from,
      nurUpdates = map protoToUpdate $ toList upds 
    }