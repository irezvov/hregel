module Concurrency.Hregel.Messages.Types (
  nodeToProto,
  protoToNode
) where

import Data.Foldable (toList)
import Data.Sequence (fromList)
import Data.Maybe (fromMaybe)
import Text.ProtocolBuffers.Basic (uFromString, uToString)

import Concurrency.Hregel.Core.Types

import qualified Proto.Hregel.Node as HNode

protoToNode :: HNode.Node -> Node
protoToNode (HNode.Node addr nid parts) = Node {
  nAddress = uToString addr,
  nId = fromIntegral $ fromMaybe (-1) nid,
  nPartitions = map fromIntegral $ toList parts
}

nodeToProto :: Node -> HNode.Node
nodeToProto (Node addr nid parts) = HNode.Node {
  HNode.address = uFromString addr,
  HNode.id = Just $ fromIntegral $ toInteger nid,
  HNode.partitions = fromList $ map (fromIntegral . toInteger) parts
}
