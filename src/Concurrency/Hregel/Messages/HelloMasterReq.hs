module Concurrency.Hregel.Messages.HelloMasterReq (
  HelloMasterReq(..)
  ) where

import Text.ProtocolBuffers.Basic (uFromString, uToString, defaultValue)

import Concurrency.Hregel.Messages.Message
import qualified Proto.Hregel.HelloMasterReq as Proto

data HelloMasterReq = HelloMasterReq { hmAddress :: String }
                    deriving (Show, Eq)

instance Message HelloMasterReq where
  pack (HelloMasterReq addr) = defaultPack $ defaultValue {
    Proto.address = uFromString addr
  }
  unpack = defaultUnpack $ \(Proto.HelloMasterReq addr) ->
    HelloMasterReq $ uToString addr
