module Concurrency.Hregel.Messages.InputReq (
  InputReq(..)
) where

import Data.Foldable (toList)
import Data.Sequence (fromList)

import Text.ProtocolBuffers.Basic (defaultValue)

import Concurrency.Hregel.Core.Types hiding (Message)
import Concurrency.Hregel.Messages.Message 
import Concurrency.Hregel.Messages.Data

import qualified Proto.Hregel.InputReq as Proto

data InputReq = InputReq {
    irPartition :: PartitionId,
    irVertices :: [VertexDescription]
  }
  deriving (Show, Eq)

instance Message InputReq where
  pack (InputReq partId vertices) = defaultPack $ defaultValue {
    Proto.partition = fromIntegral partId,
    Proto.vertices = fromList $ map vertexToProto vertices
  }

  unpack = defaultUnpack $ \(Proto.InputReq partId vertices) ->
    InputReq {
      irPartition = fromIntegral partId,
      irVertices = map protoToVertex $ toList vertices
    }
