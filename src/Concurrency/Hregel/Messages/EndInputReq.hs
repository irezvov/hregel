module Concurrency.Hregel.Messages.EndInputReq (
  EndInputReq(..)
) where

import Text.ProtocolBuffers.Basic (defaultValue)

import Concurrency.Hregel.Core.Types hiding (Message)
import Concurrency.Hregel.Messages.Message 

import qualified Proto.Hregel.EndInputReq as Proto

data EndInputReq = EndInputReq {
    eirPartition :: PartitionId
  }
  deriving (Show, Eq)

instance Message EndInputReq where
  pack (EndInputReq partId) = defaultPack $ defaultValue {
    Proto.partition = fromIntegral partId
  }

  unpack = defaultUnpack $ \(Proto.EndInputReq partId) ->
    EndInputReq {
      eirPartition = fromIntegral partId
    }
    