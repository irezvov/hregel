module Concurrency.Hregel.Messages.EndSuperstepRes (
  EndSuperstepRes(..)
) where

import Text.ProtocolBuffers.Basic (defaultValue)

import Concurrency.Hregel.Core.Types hiding (Message)
import Concurrency.Hregel.Messages.Message 

import qualified Proto.Hregel.EndSuperstepRes as Proto

data EndSuperstepRes = EndSuperstepRes {
    esrNode :: NodeId,
    esrActive :: Int
  }
  deriving (Show, Eq)

instance Message EndSuperstepRes where
  pack (EndSuperstepRes nodeId active) = defaultPack $ defaultValue {
    Proto.from = fromIntegral nodeId,
    Proto.activeCount = fromIntegral active
  }

  unpack = defaultUnpack $ \(Proto.EndSuperstepRes nodeId active) ->
    EndSuperstepRes {
      esrNode = fromIntegral nodeId,
      esrActive = fromIntegral active
    }
 
