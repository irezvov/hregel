module Concurrency.Hregel.Messages.LogReq (
  LogReq(..),
) where

import Text.ProtocolBuffers.Basic (uFromString, uToString, defaultValue)

import Concurrency.Hregel.Messages.Message
import qualified Concurrency.Hregel.Utils as Utils
import qualified Concurrency.Hregel.Core.Types as Types
import qualified Proto.Hregel.LogReq as Proto
import qualified Proto.Hregel.LogReq.Level as Proto.Level
import qualified Data.Time.Clock as Clock
import Data.Time.LocalTime ()

data LogReq = LogReq {
  lTime :: Clock.UTCTime,
  lMsg :: String,
  lLevel :: Maybe Types.LogLevel,
  lNodeId :: Maybe Types.NodeId
} deriving (Eq, Show)

instance Message LogReq where
  pack (LogReq time message lvl node_id) =
    defaultPack $ defaultValue {
      Proto.time = Utils.utcToTimestamp time,
      Proto.message = uFromString message,
      Proto.level = fmap castLevels lvl,
      Proto.node_id = fmap fromIntegral node_id
    }
  unpack = defaultUnpack $ \(Proto.LogReq time message lvl node_id) ->
    LogReq {
      lTime = Utils.timestampToUtc time,
      lMsg = uToString message,
      lLevel = fmap castProtoLevels lvl,
      lNodeId = fmap (Types.NodeId . fromIntegral) node_id
    }

castLevels :: Types.LogLevel -> Proto.Level.Level
castLevels lvl = case lvl of
  Types.LogDebug -> Proto.Level.Debug
  Types.LogInfo -> Proto.Level.Info
  Types.LogWarning -> Proto.Level.Warning
  Types.LogError -> Proto.Level.Error

castProtoLevels :: Proto.Level.Level -> Types.LogLevel
castProtoLevels lvl = case lvl of
  Proto.Level.Debug -> Types.LogDebug
  Proto.Level.Info -> Types.LogInfo
  Proto.Level.Warning -> Types.LogWarning
  Proto.Level.Error -> Types.LogError
