module Concurrency.Hregel.Messages.Message (
  Message,
  pack,
  unpack,
  defaultPack,
  defaultUnpack
) where

import qualified Data.ByteString as BS
import qualified Data.ByteString.Lazy as Lazy

import Text.ProtocolBuffers.WireMessage (Wire, messagePut, messageGet)
import Text.ProtocolBuffers.Reflections (ReflectDescriptor)

import qualified Proto.Hregel.Message.Type as Msg ()

class Message a where
  pack   :: a -> BS.ByteString
  unpack :: BS.ByteString -> Either String a

defaultPack :: (ReflectDescriptor msg, Wire msg) => msg -> BS.ByteString
defaultPack = Lazy.toStrict . messagePut

defaultUnpack :: (ReflectDescriptor msg, Wire msg, Message a) => 
                 (msg -> a) -> BS.ByteString -> Either String a
defaultUnpack f bin = do
  (msg, _rest) <- messageGet $ Lazy.fromStrict bin
  return $ f msg
