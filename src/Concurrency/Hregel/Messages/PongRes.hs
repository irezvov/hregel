module Concurrency.Hregel.Messages.PongRes (
  PongRes(..)
  ) where

import Text.ProtocolBuffers.Basic (defaultValue)

import Concurrency.Hregel.Core.Types (NodeId)
import Concurrency.Hregel.Messages.Message
import qualified Proto.Hregel.PongRes as Proto

data PongRes = PongRes { pNodeId :: NodeId }
  deriving (Show, Eq)

instance Message PongRes where
  pack (PongRes nodeId) = 
    defaultPack $ defaultValue { Proto.from = fromIntegral nodeId }
  unpack = defaultUnpack $ \proto -> 
    PongRes $ fromIntegral $ Proto.from proto
