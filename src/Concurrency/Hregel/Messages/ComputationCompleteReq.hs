module Concurrency.Hregel.Messages.ComputationCompleteReq (
  ComputationCompleteReq(..)
) where

import Text.ProtocolBuffers.Basic (defaultValue)

import Concurrency.Hregel.Messages.Message
import qualified Proto.Hregel.ComputationCompleteReq as Proto

data ComputationCompleteReq = ComputationCompleteReq
  deriving (Show, Eq)

fromProto :: Proto.ComputationCompleteReq -> ComputationCompleteReq
fromProto = const ComputationCompleteReq

instance Message ComputationCompleteReq where
  pack ComputationCompleteReq = 
    defaultPack (defaultValue :: Proto.ComputationCompleteReq)
  unpack = defaultUnpack fromProto
