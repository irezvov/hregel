module Concurrency.Hregel.Messages.PingReq (
  PingReq(..)
) where

import Text.ProtocolBuffers.Basic (defaultValue)

import Concurrency.Hregel.Messages.Message
import qualified Proto.Hregel.PingReq as Proto

data PingReq = PingReq
  deriving (Show, Eq)

fromProto :: Proto.PingReq -> PingReq
fromProto = const PingReq

instance Message PingReq where
  pack PingReq = 
    defaultPack (defaultValue :: Proto.PingReq)
  unpack = defaultUnpack fromProto
