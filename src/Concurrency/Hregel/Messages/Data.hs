module Concurrency.Hregel.Messages.Data (
  edgeToProto,
  protoToEdge,
  vertexToProto,
  protoToVertex
) where

import Data.Foldable (toList)
import Data.Sequence (fromList)
import Data.Maybe (fromMaybe)
import qualified Data.ByteString.Lazy.Char8 as LC

import Text.ProtocolBuffers.Basic (uFromString, uToString, defaultValue)
import Data.Serialize ()

import Concurrency.Hregel.Core.Types
import qualified Proto.Hregel.Vertex as VP
import qualified Proto.Hregel.Edge as EP

edgeToProto :: EdgeDescription -> EP.Edge
edgeToProto (ED to val) = defaultValue {
    EP.to = uFromString to,
    EP.value = Just $ LC.pack val
  }

protoToEdge :: EP.Edge -> EdgeDescription
protoToEdge (EP.Edge to valData) =
  ED (uToString to) $ LC.unpack (fromMaybe LC.empty valData)

vertexToProto :: VertexDescription -> VP.Vertex
vertexToProto (VD vid value edges) = VP.Vertex {
    VP.id = uFromString vid,
    VP.value = LC.pack value,
    VP.edges = fromList $ map edgeToProto edges
  }

protoToVertex :: VP.Vertex -> VertexDescription
protoToVertex (VP.Vertex vid value edges) = VD {
    vdId = uToString vid,
    vdValue = LC.unpack value,
    vdEdges = map protoToEdge $ toList edges
  }
