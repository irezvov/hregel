module Concurrency.Hregel.Messages.HelloMasterRes ( 
  HelloMasterRes(..)
  ) where

import Data.Foldable (toList)
import Data.Sequence (fromList)

import Text.ProtocolBuffers.Basic (
    uFromString, uToString, defaultValue
  )
import Concurrency.Hregel.Core.Types (Node(..))
import Concurrency.Hregel.Messages.Message
import Concurrency.Hregel.Messages.Types
import qualified Proto.Hregel.HelloMasterRes as Proto


data HelloMasterRes = HelloMasterRes { 
      hmNode :: Node,
      hmNodes :: [Node],
      hmLogServer :: Maybe String
    }
  deriving (Show, Eq)

instance Message HelloMasterRes where
  pack (HelloMasterRes node nodes logger) = 
    defaultPack $ defaultValue {
        Proto.node = nodeToProto node,
        Proto.nodes = fromList $ map nodeToProto nodes,
        Proto.logServer = fmap uFromString logger 
      }
  unpack = 
    defaultUnpack $ \(Proto.HelloMasterRes node nodes logger) ->
    HelloMasterRes {
      hmNode = protoToNode node,
      hmNodes = map protoToNode $ toList nodes,
      hmLogServer = fmap uToString logger
    }
