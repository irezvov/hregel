module Main where

import System.IO
import Control.Monad (forever, when)
import Data.Maybe (fromMaybe)
import qualified Data.Map.Lazy as Map

import Concurrency.Hregel.Core.Types
import Concurrency.Hregel.Core.Connection (connection)
import qualified Concurrency.Hregel.Core.JsonReader as R
import qualified Concurrency.Hregel.Core.Reader as Reader
import qualified Concurrency.Hregel.Core.Log as Log
import qualified Concurrency.Hregel.Core.Partitioner as P
import Concurrency.Hregel.Master.Config
import Concurrency.Hregel.Master.State

import Concurrency.Hregel.Messages.NetMessage

logFileName :: Maybe NodeId -> FilePath
logFileName nid = case nid of
  Nothing -> "log_common"
  Just (NodeId n) -> "log_" ++ (show n)

handleMessage :: NetMessage -> Master ()
handleMessage message = case message of
  HelloMasterReqMsg (HelloMasterReq nodeAddr) -> do
    ns <- nodes
    node <- newNode nodeAddr
    let msg = HelloMasterResMsg $ HelloMasterRes node ns Nothing
    send (nId node) msg
    -- Check if all nodes have been connected.  Iff so - send data to them.
    nodesNum <- nodesNumber
    cfg <- config
    when (nodesNum == cfgWorkersNumber cfg) $
      sendDataToNodes >> startNextSuperstep
  LogReqMsg (LogReq _ msg _ nid) ->
    liftIO $ withFile (logFileName nid) AppendMode $ flip hPutStrLn msg
  EndSuperstepResMsg (EndSuperstepRes node active) -> do
    liftIO $ Log.logWith "Master" LogInfo "Received EndSuperstepResMsg: "
      $ "(" ++ (show node) ++ "," ++ (show active) ++ ")"
    nodeFinished active
    comp <- getComputation
    case comp of
      Finished -> sendComputationComplete
      Stopped -> startNextSuperstep
      _ -> do
        liftIO $ Log.log "Master" LogError "Unacceptable computation state"
        liftIO $ Log.logWith "Master" LogError "Message from" $ show node
  InputReqMsg (InputReq node vertices) -> do
    liftIO $ Log.logWith "Master" LogInfo "Received vertices from " $ show node
    addGraph vertices
  _ -> liftIO $ Log.logWith "Master" LogWarning "Unknown message: " message

main :: IO ()
main = do
  cfg <- getConfig
  conn <- connection $ "*:" ++ (show . cfgPort) cfg
  Log.addComponent "Master"
  Log.log "Master" LogInfo "Starting master..."
  master conn cfg $ forever $ do
    e <- event
    case e of
      Net msg -> 
        handleMessage msg
      NodeDown nid ->
        liftIO $ Log.logWith "Master" LogError "Node is down" nid

splitByPartition :: [VertexDescription] ->
                    Int ->
                    Map.Map PartitionId [VertexDescription]
splitByPartition verts num = foldl helper Map.empty verts
  where
    helper m vertexDesc =
      let key = P.defaultPartition (vdId vertexDesc) num in
      let vs = fromMaybe [] $ Map.lookup key m in
      Map.insert key (vertexDesc : vs) m

nodeByPartition :: PartitionsMap -> PartitionId -> NodeId
nodeByPartition m partition = fromMaybe (NodeId 0) $
  Map.foldlWithKey helper Nothing m
  where
    helper (Just node) _ _ = Just node
    helper _ node partitions = case elem partition partitions of
      True -> Just node
      False -> Nothing

sendDataToNodes :: Master ()
sendDataToNodes = do
  cfg <- config
  liftIO $ Log.logWith "Master" LogInfo "Using input file " $ cfgInputFileName cfg
  verts <- liftIO $ Reader.pull R.JsonReader $ cfgInputFileName cfg
  let parts = splitByPartition verts $ cfgWorkersNumber cfg * cfgCapacity cfg
  partitions <- partitionsMap
  liftIO $ Log.logWith "Master" LogDebug "Partitions map: " partitions
  Map.foldlWithKey (sendMsgHelper partitions) (return ()) parts
  where
    sendMsgHelper partitions prevIO partition verts =
      let node = nodeByPartition partitions partition in do
      prevIO
      liftIO $ Log.log "Master" LogDebug $
        "Partition " ++ (show partition) ++ " is on node " ++ (show node)
      send node $ InputReqMsg $ InputReq partition verts

sendMsg :: NetMessage -> Master a -> Node -> Master ()
sendMsg msg someIO node = someIO >> send (nId node) msg

startNextSuperstep :: Master ()
startNextSuperstep = do
  ns <- nodes
  let msg = SuperstepReqMsg SuperstepReq
  foldl (sendMsg msg) (return ()) ns
  incSuperstep
  liftIO $ Log.log "Master" LogInfo "Sent StartSuperstep messages"

sendComputationComplete :: Master ()
sendComputationComplete = do
  ns <- nodes
  let msg = ComputationCompleteReqMsg ComputationCompleteReq
  foldl (sendMsg msg) (return ()) ns
  liftIO $ Log.log "Master" LogInfo "Sent ComputationComplete messages"
