module Concurrency.Hregel.Master.Config (
  Config(..),
  getConfig
) where

import System.Environment (getArgs)
import System.Console.GetOpt
import Data.Maybe (fromMaybe, listToMaybe)
import Control.Monad (when, join)

data Config = Config {
  cfgInputFileName :: String,
  cfgOutputFileName :: String,
  cfgWorkersNumber :: Int,
  cfgPort :: Int,
  cfgCapacity :: Int,
  cfgLogServer :: Maybe String
}

options :: [OptDescr (Config -> Config)]
options = [
  Option ['i'] ["input"]
    (OptArg inp "<file_name>")
    "input file name -- optional",
  Option ['o'] ["output"]
    (OptArg outp "<file_name>")
    "output file name -- optional",
  Option ['w'] ["workers-number"]
    (OptArg (\ws cfg -> cfg { cfgWorkersNumber = getNumber 1 ws }) "NUMBER")
    "number of running workers -- optional",
  Option ['p'] ["port"]
    (OptArg (\port cfg -> cfg { cfgPort = getNumber 1488 port }) "NUMBER")
    "listening port -- optional",
  Option ['c'] ["capacity"]
    (OptArg (\cap cfg -> cfg { cfgCapacity = getNumber 1 cap }) "NUMBER")
    "number of partitions per worker -- optional",
  Option ['l'] ["log-server"]
    (OptArg (\addr cfg -> cfg { cfgLogServer = addr }) "ADDR:PORT")
    "address of log server"
  ]

inp,outp :: Maybe String -> Config -> Config
inp file_name cfg = cfg { cfgInputFileName = fromMaybe "input" file_name }
outp file_name cfg = cfg { cfgOutputFileName = fromMaybe "output" file_name }

getNumber :: Int -> Maybe String -> Int
getNumber def_val str_val =
  let readToMaybe = fmap (listToMaybe . reads) in
  let (ws, _) = fromMaybe (def_val, "") $ join $ readToMaybe str_val in
  ws

defaultConfig :: Config
defaultConfig = Config "input" "output" 1 1488 1 Nothing

getConfig :: IO Config
getConfig = do
  args <- getArgs
  when (null args) $ ioError $ userError usage
  case getOpt Permute options args of
    (o, _n, []) -> return $ foldl (flip id) defaultConfig o
    (_, _, errs) -> ioError $ userError $ concat errs ++ usage
  where usage = usageInfo "" options
