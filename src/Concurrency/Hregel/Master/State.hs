module Concurrency.Hregel.Master.State (
  Master,
  PartitionsMap,
  Computation(..),
  Event(..),
  master,
  send,
  config,
  newNode,
  nodes,
  nodesNumber,
  partitionsMap,
  getSuperstep,
  incSuperstep,
  getComputation,
  nodeFinished,
  addGraph,
  event,
  liftIO -- reimport from State
) where

import Data.Maybe (isJust, fromMaybe)
import Data.List (find)
import qualified Data.Map as Map
import Control.Monad.State
import Control.Concurrent.Chan
import Control.Concurrent.MVar
import Control.Concurrent (forkIO, threadDelay)

import Concurrency.Hregel.Core.Types
import Concurrency.Hregel.Messages.NetMessage
import qualified Concurrency.Hregel.Core.Connection as Conn
import qualified Concurrency.Hregel.Core.Log as Log
import qualified Concurrency.Hregel.Master.Config as Config
import qualified Concurrency.Hregel.Core.Reader as Reader
import qualified Concurrency.Hregel.Core.JsonReader as R

type Master = StateT MasterState IO

type PartitionsMap = Map.Map NodeId [PartitionId]

data Computation = NotStarted | Running | Stopped | Finished

data WorkersState = WS {
    wsActiveVertices :: Int, -- Number of vertices active at end of superstep.
    wsFinishedNodes :: Int -- Number of workers, finished superstep.
  }

data MasterState = St {
    stNodes :: [Node],
    stConn :: Conn.Connection,
    stConfig :: Config.Config,
    stPartitionsMap :: PartitionsMap,
    stSuperstep :: Int,
    stComputation :: Computation,
    stWorkersState :: WorkersState,
    stResultGraph :: [VertexDescription],
    stEvents :: Chan Event,
    stNodesStatus :: MVar (Map.Map NodeId NodeStatus)
  }

data Event = Net NetMessage
           | NodeDown NodeId
  deriving (Show,Eq)

data NodeStatus = Active 
                | Down
                | Waiting
  deriving (Eq, Show)

event :: Master Event
event = do
  es <- gets stEvents
  lift $ readChan es

send :: NodeId -> NetMessage -> Master ()
send nodeId msg = do
  conn <- gets stConn
  lift $ Conn.send conn (Worker nodeId) msg

config :: Master Config.Config
config = gets stConfig

nodes :: Master [Node]
nodes = gets stNodes

nodesNumber :: Master Int
nodesNumber = do
  ns <- nodes
  return $ length ns

partitionsMap :: Master PartitionsMap
partitionsMap = gets stPartitionsMap

getSuperstep :: Master Int
getSuperstep = gets stSuperstep

incSuperstep :: Master ()
incSuperstep = do
  superstep <- gets stSuperstep
  modify $ \st' -> st' {
    stSuperstep = superstep + 1,
    stComputation = Running,
    stWorkersState = WS 0 0
  }

getComputation :: Master Computation
getComputation = gets stComputation

setComputation :: Computation -> Master ()
setComputation comp = modify $ \st' -> st' { stComputation = comp }

nodeFinished :: Int -> Master ()
nodeFinished active = do
  ws <- gets stWorkersState
  let finished = wsFinishedNodes ws + 1
  modify $ \st' -> st' {
    stWorkersState = WS {
      wsActiveVertices = (wsActiveVertices ws) + active,
      wsFinishedNodes = finished
    }
  }
  ns <- nodes
  when (finished == length ns) checkComputation

addGraph :: [VertexDescription] -> Master ()
addGraph vs = do
  ws <- gets stWorkersState
  let finished = (wsFinishedNodes ws) + 1
  graph <- gets stResultGraph
  modify $ \st' -> st' {
    stWorkersState = ws {
      wsFinishedNodes = finished
    },
    stResultGraph = vs ++ graph
  }
  ns <- nodes
  cfg <- gets stConfig
  when (finished == length ns * Config.cfgCapacity cfg) saveGraph

saveGraph :: Master ()
saveGraph = do
  cfg <- gets stConfig
  graph <- gets stResultGraph
  let fileName = Config.cfgOutputFileName cfg
  lift $ Reader.push R.JsonReader fileName graph
  lift $ Log.log "Master" LogInfo "Saved graph to output"

checkComputation :: Master ()
checkComputation = do
  ws <- gets stWorkersState
  if wsActiveVertices ws == 0 then do
    setComputation Finished
    modify $ \st' -> st' {
      stWorkersState = WS 0 0
    }
  else setComputation Stopped

newNode :: String -> Master Node
newNode nodeAddr = do
  ns <- nodes
  conn <- gets stConn
  -- maybe we have already have this address?
  let old = find ((== nodeAddr) . nAddress) ns
  let nodeId = maybe (fromIntegral $ length ns + 1) nId old
  when (isJust old) $ do
    -- remove from list of nodes
    modify $ \st' -> st' { stNodes = filter ((== nodeId) . nId) ns }
    lift $ Log.logWith "Master" LogInfo "Reconnect for " nodeAddr
  lift $ Conn.connect conn (Worker nodeId) nodeAddr
  partitions <- gets stPartitionsMap
  let node = Node nodeAddr nodeId $ fromMaybe [] (Map.lookup nodeId partitions)
  nodesStatus <- gets stNodesStatus
  lift $ modifyMVar_ nodesStatus $ return . Map.insert nodeId Active
  modify $ \st' -> st' { stNodes = node : ns }
  pingTimer nodeId 3
  return node

pingTimer :: NodeId -> Int -> Master ()
pingTimer nid time = do
  nodesStatus <- gets stNodesStatus
  eventChan <- gets stEvents
  conn <- gets stConn
  lift $ void $ forkIO $ routine nodesStatus eventChan conn
  where 
    routine nodesStatus eventChan conn = do
      threadDelay $ time * 1000 * 1000
      go <- modifyMVar nodesStatus $ \ns ->
        case Map.lookup nid ns of
          Nothing ->
            return (ns, False)
          Just Active -> do
            Conn.send conn (Worker nid) $ PingReqMsg PingReq
            return $ (Map.insert nid Waiting ns, True)
          Just Waiting -> do
            writeChan eventChan $ NodeDown nid
            return $ (Map.delete nid ns, False)
          Just Down -> do
            writeChan eventChan $ NodeDown nid
            Log.logWith "Master" LogError "Node down twice" nid
            return $ (Map.delete nid ns, False)
      when go $ routine nodesStatus eventChan conn

master :: Conn.Connection -> Config.Config -> Master () -> IO ()
master conn cfg comp = do
  eventChan <- newChan
  ns <- newMVar Map.empty
  let workers_num = Config.cfgWorkersNumber cfg
  let workers_cap = Config.cfgCapacity cfg
  let parts_map = partitionIdSparse workers_num workers_cap
  forkIO $ forever $ do
    msg <- Conn.receive conn
    case msg of
      PongResMsg (PongRes from) -> modifyMVar_ ns $ \nodesMap ->
        return $ Map.insert from Active nodesMap
      _ ->
        writeChan eventChan $ Net msg
  evalStateT comp $
    St [] conn cfg parts_map 1 NotStarted (WS 0 0) [] eventChan ns

partitionIdSparse :: Int -> Int -> PartitionsMap
partitionIdSparse num capacity = helper num capacity Map.empty where
  helper :: Int -> Int -> PartitionsMap -> PartitionsMap
  helper 0 _ m = m
  helper n cap m =
    let from = PartitionId $ (n - 1) * cap in
    let to = PartitionId $ n * cap in
    helper (n - 1) cap $ Map.insert (NodeId n) [from..to - 1] m
