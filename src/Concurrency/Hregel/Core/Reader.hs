module Concurrency.Hregel.Core.Reader where

import qualified System.FilePath.Posix as Posix

import qualified Concurrency.Hregel.Core.Types as T

class Reader a where
  pull :: a -> Posix.FilePath -> IO [T.VertexDescription]
  push :: a -> Posix.FilePath -> [T.VertexDescription] -> IO ()
