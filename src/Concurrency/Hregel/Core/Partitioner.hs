module Concurrency.Hregel.Core.Partitioner where

import qualified Data.Hash as Hash
import qualified Concurrency.Hregel.Core.Types as T

defaultPartition :: T.VertexId -> Int -> T.PartitionId
defaultPartition nid num = T.PartitionId $
  (fromIntegral $ Hash.asWord64 $ Hash.hash nid) `mod` num
