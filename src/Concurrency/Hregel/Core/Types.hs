{-# LANGUAGE GeneralizedNewtypeDeriving, InstanceSigs, OverloadedStrings #-}
module Concurrency.Hregel.Core.Types where

import Control.Applicative ((<$>), (<*>))
import Control.Monad (mzero)
import Data.Maybe (fromMaybe)
import Data.Aeson as Aeson

type VertexId = String

data Edge e = Edge {
    eTo :: VertexId,
    eValue :: e
  }

data Message m = Message {
    mTo :: VertexId,
    mValue :: m
  }

data Vertex v e = Vertex {
    vId :: VertexId,
    vValue :: v,
    vEdges :: [Edge e]
  }

data EdgeDescription = ED {
    edTo :: VertexId,
    edValue :: String
  } deriving (Show, Eq)

instance Aeson.FromJSON EdgeDescription where
  parseJSON (Aeson.Object v) = ED <$> v .: "to" <*> v .: "value"
  parseJSON _                = mzero

instance Aeson.ToJSON EdgeDescription where
  toJSON (ED to value) =
      Aeson.object ["to" .= to,
                    "value" .= value]

data VertexDescription = VD {
    vdId :: VertexId,
    vdValue :: String,
    vdEdges :: [EdgeDescription]
  } deriving (Show, Eq)

instance Aeson.FromJSON VertexDescription where
  parseJSON (Aeson.Object v) =
      VD <$> v .: "id" <*> (fromMaybe "" <$> v .: "value") <*> v .: "edges"
  parseJSON _                = mzero

instance Aeson.ToJSON VertexDescription where
  toJSON (VD vertexId value edges) =
      Aeson.object ["id" .= vertexId,
                    "value" .= value,
                    "edges" .= edges]

newtype NodeId = NodeId Int 
  deriving (Eq, Show, Ord, Num, Real, Enum, Integral)

newtype PartitionId = PartitionId Int 
  deriving (Eq, Show, Ord, Num, Real, Enum, Integral)

data Node = Node { 
    nAddress :: String,
    nId :: NodeId,
    nPartitions :: [PartitionId]
  }
  deriving (Show, Eq)

data Destination = Master | Worker NodeId
  deriving (Eq, Ord)

data LogLevel = LogDebug | LogInfo | LogWarning | LogError
  deriving (Eq, Ord, Show)
