module Concurrency.Hregel.Core.Connection (
  Connection,
  connection,
  connect,
  send,
  receive
) where

import Control.Concurrent.Chan
import Control.Concurrent (forkIO)
import Control.Monad ()
import Control.Monad.State
import Control.Applicative ((<$>))
import Data.Maybe (isJust, fromJust)
import qualified Data.Map as Map

import System.ZMQ4.Monadic hiding (connect, receive, send)
import qualified System.ZMQ4.Monadic as Zmq

import Concurrency.Hregel.Messages.Message
import Concurrency.Hregel.Messages.NetMessage
import Concurrency.Hregel.Core.Types

data Connection =
  Connection (Chan NetMessage) (Chan Command)

data Command = Send Destination NetMessage
             | Add Destination String

connect :: Connection -> Destination -> String -> IO ()
connect (Connection _ out) dist addr = 
  writeChan out $ Add dist addr

send :: Connection
     -> Destination
     -> NetMessage
     -> IO ()
send (Connection _ out) dist msg =
  writeChan out $ Send dist msg

receive :: Connection -> IO NetMessage
receive (Connection inp _) = readChan inp

connection :: String -> IO Connection
connection addr = do
  inp <- newChan
  out <- newChan
  forkIO $ output out
  forkIO $ input addr inp
  return $ Connection inp out

input :: String -> Chan NetMessage -> IO ()
input addr inp = runZMQ $ do
  s <- socket Pull
  bind s $ "tcp://" ++ addr
  forever $ do
    msg <- unpack <$> Zmq.receive s
    case msg of 
      Left err -> liftIO $ putStrLn $ "Message unpack err: " ++ err
      Right msg' -> liftIO $ writeChan inp msg'

output :: Chan Command -> IO ()
output out = runZMQ $ loop Map.empty
  where 
    loop nodes = do
      cmd <- liftIO $ readChan out
      case cmd of
        Add dest addr -> do
          s <- socket Push
          Zmq.connect s $ "tcp://" ++ addr
          let old = Map.lookup dest nodes
          when (isJust old) $ do
            close $ fromJust old
          loop $ Map.insert dest s nodes
        Send dest msg -> do
          let s = Map.lookup dest nodes
          when (isJust s) $ do
            Zmq.send (fromJust s) [] $ pack msg
          loop nodes
