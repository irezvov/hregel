module Concurrency.Hregel.Core.Log (
  addComponent,
  addComponentEx,
  log,
  logWith
) where

import Prelude hiding (log)
import System.Log.Logger
import qualified System.Log as SLog
import qualified System.Log.Handler as SLHandler
import qualified System.Log.Handler.Simple as SLSimple
import qualified System.Log.Formatter as SLFormatter
import qualified GHC.IO.Handle.FD as FD
import qualified Data.Time.Clock as Time

import Concurrency.Hregel.Core.Connection
import Concurrency.Hregel.Core.Types
import Concurrency.Hregel.Messages.NetMessage

fromLevel :: LogLevel -> SLog.Priority
fromLevel level = case level of
  LogDebug -> SLog.DEBUG
  LogInfo -> SLog.INFO
  LogWarning -> SLog.WARNING
  LogError -> SLog.ERROR

toLevel :: SLog.Priority -> LogLevel
toLevel priority = case priority of
  SLog.DEBUG -> LogDebug
  SLog.INFO -> LogInfo
  SLog.NOTICE -> LogInfo
  SLog.WARNING -> LogWarning
  _ -> LogError

log :: String -> LogLevel -> String -> IO ()
log name level msg = logM name (fromLevel level) msg

logWith :: (Show a) => String -> LogLevel -> String -> a -> IO ()
logWith name level msg payload =
  log name level $ msg ++ " Payload: " ++ show payload

defaultFormat :: String
defaultFormat = "[$time - $prio] $loggername : $msg"

silentRootLogger :: IO ()
silentRootLogger = updateGlobalLogger rootLoggerName (setHandlers [NullHandler])

addComponent :: String -> IO ()
addComponent name = do
  silentRootLogger
  h <- SLSimple.streamHandler FD.stderr DEBUG >>= \lh -> return $
    SLHandler.setFormatter lh (SLFormatter.simpleLogFormatter defaultFormat)
  updateGlobalLogger name (addHandler h . setLevel DEBUG)

addComponentEx :: String -> NodeId -> Connection -> IO ()
addComponentEx name nid conn = do
  silentRootLogger
  let formatter = SLFormatter.simpleLogFormatter defaultFormat
  let sender = SLHandler.setFormatter (defaultSendHandler nid conn) formatter
  updateGlobalLogger name (addHandler sender . setLevel DEBUG)

data NullHandler = NullHandler

instance SLHandler.LogHandler NullHandler where
  setLevel _ _ = NullHandler
  getLevel _ = SLog.DEBUG
  setFormatter _ _ = NullHandler
  getFormatter _ = SLFormatter.nullFormatter
  handle _ _ _ = return ()
  emit _ _ _ = return ()
  close _ = return ()

data SendHandler = SendHandler {
  shConn :: Connection,
  shLevel :: LogLevel,
  shFormatter :: SLFormatter.LogFormatter SendHandler,
  shId :: NodeId
}

instance SLHandler.LogHandler SendHandler where
  setLevel sender priority = sender {
    shLevel = toLevel priority
  }
  getLevel = fromLevel . shLevel
  setFormatter sender formatter = sender {
    shFormatter = formatter
  }
  getFormatter = shFormatter
  handle = sendMsg
  emit = sendMsg
  close _ = return ()

sendMsg :: SendHandler -> SLog.LogRecord -> String -> IO ()
sendMsg sender log_rec name = do
  current_time <- Time.getCurrentTime
  formatted_msg <- shFormatter sender sender log_rec name
  send (shConn sender) Master $ LogReqMsg $ LogReq {
    lTime = current_time,
    lMsg = formatted_msg,
    lLevel = Just $ toLevel $ fst log_rec,
    lNodeId = Just $ shId sender
  }

defaultSendHandler :: NodeId -> Connection -> SendHandler
defaultSendHandler node_id conn =
  SendHandler {
    shConn = conn,
    shLevel = LogDebug,
    shFormatter = SLFormatter.nullFormatter,
    shId = node_id
  }
