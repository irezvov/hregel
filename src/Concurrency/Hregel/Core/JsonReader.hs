{-# LANGUAGE InstanceSigs, OverloadedStrings #-}
module Concurrency.Hregel.Core.JsonReader where

import System.IO ()
import qualified System.FilePath.Posix as Posix
import qualified Data.ByteString.Lazy.Char8 as Lazy
import qualified Concurrency.Hregel.Core.Types as T
import qualified Concurrency.Hregel.Core.Reader as R
import Data.Aeson as Aeson

data JsonReader = JsonReader

instance R.Reader JsonReader where
  pull :: JsonReader -> Posix.FilePath -> IO [T.VertexDescription]
  pull _ path = do
    contents <- Lazy.readFile path
    return $ process $ Aeson.decode contents
    where process (Just l) = l
          process Nothing = []

  push :: JsonReader -> Posix.FilePath -> [T.VertexDescription] -> IO ()
  push _ path vs = do
    let body = Lazy.intercalate "," $ map Aeson.encode vs
    Lazy.writeFile path $ flip Lazy.append "]" $ Lazy.append "[" body
